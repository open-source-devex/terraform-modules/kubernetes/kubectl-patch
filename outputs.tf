output "patch_triggers" {
  # These resources should be mutually exclusive
  value = merge(
    try(null_resource.patch_with_errors[0].triggers, {}),
    try(null_resource.patch_without_errors[0].triggers, {}),
  )
}
