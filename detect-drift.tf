locals {
  detected_drift = try(data.external.detect_drift[0].result["changed"], "NA")

  triggers_detect_drift = var.detect_drift ? {
    # use of timestamp() makes sure resources are triggered even when they did not change since last run
    drift = "drift detected :: ${local.detected_drift == "true" ? "true ${timestamp()}" : local.detected_drift}"
  } : {}
}

data "external" "detect_drift" {
  count = var.detect_drift ? 1 : 0

  program = ["${path.module}/scripts/patch-dry-run.sh"]

  query = {
    manifest          = var.patch_manifest
    kubeconfig        = var.kubeconfig
    namespace         = var.namespace
    patch_object_type = var.patch_object_type
    patch_object_name = var.patch_object_name
    patch_type        = var.patch_type
  }
}
