###################################################################
# Example assumes there is a default/test configmap to be patched
# and a default/alpine-pause daemonset to be restarted.
# See at the bottom of the file how to create those requirements.
#
module "patch_manifest" {
  source = "../../"

  enabled = true

  kubeconfig = "./kube/config"

  namespace         = "default"
  patch_object_name = "test"
  patch_object_type = "cm"
  patch_manifest = jsonencode({
    metadata = {
      labels = {
        foo = "bar"
      }
    }
  })

  silence_patch_errors = false
  detect_drift         = true

  rollout_restart               = true
  rollout_restart_k8s_resources = ["ds/alpine-pause"]

  patch_trigger = "apply when this changes"


}


########
# Create test cm
#
# $ kubectl create cm test
#
#######
#
# Create alpine-pause ds
#
# $ kubectl apply -f alpine-pause-ds.yaml
#
/*
# alpine-pause-ds.yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: alpine-pause
spec:
  selector:
    matchLabels:
      name: alpine-pause
  template:
    metadata:
      labels:
        name: alpine-pause
    spec:
      containers:
        - name: alpine-pause
          image: alpine:latest
          command:
            - tail
            - -f
            - /dev/null
          resources:
            limits:
              memory: 128Mi
            requests:
              cpu: 100m
              memory: 64Mi
*/
