# kubectl-patch

Terraform module to run `kubectl patch` on `terraform apply`.

## Triggering

Currently the module triggers kubectl patch commands if
- resource was tainted in the terraform state
- input for the resource changed
- when `var.detect_drift` is set to `true` and the module detects drift in the cluster

## Usage

The simplest example reading the manifest from a file is this one 👇.
```hcl-terraform
module "patch_manifest" {
  source = "git::https://gitlab.com/open-source-devex/terraform-modules/kubernetes/kubectl-patch.git?ref=v1.0.0"

  enabled = true

  kubeconfig    = "./kube/config"
  namespace            = "default"
  patch_object_type    = "configmap"
  patch_object_name    = "some-config-map"
  manifest_patch       = yamlencode({data = { "my-file" = "file content"}})
  silence_patch_errors = false
}
```
There are [examples](./examples) showing all the available options.

## Terraform docs

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| external | n/a |
| null | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| detect\_drift | Whether to try and detect drift to trigger apply | `bool` | `false` | no |
| enabled | Whether to create the resources in the module | `bool` | `true` | no |
| kubeconfig | Value for KUBECONFIG on the kubectl environment (when `null` value is not set) | `string` | `null` | no |
| namespace | The k8s namespace where the object to patch lives | `string` | `"default"` | no |
| patch\_manifest | The k8s manifest to use in patching the object | `string` | n/a | yes |
| patch\_object\_name | The name of the specific object to be patched | `string` | n/a | yes |
| patch\_object\_type | The type of the object to be patche (eg. pod, job, etc) | `string` | n/a | yes |
| patch\_trigger | Triggers for apply, whenever the string changes apply will trigger | `string` | `" "` | no |
| patch\_type | The type of kubectl patch | `string` | `"merge"` | no |
| rollout\_restart | Whether to trigger a `kubectl rollout restart` whenever the patch is applied | `bool` | `false` | no |
| rollout\_restart\_k8s\_resources | A set of fully qualified k8s resources (as expected by `kubectl rollout restart`) to be restarted | `set(string)` | `[]` | no |
| silence\_patch\_errors | Whether the module is configured to not fail even when the kubectl patch fails | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| patch\_triggers | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
