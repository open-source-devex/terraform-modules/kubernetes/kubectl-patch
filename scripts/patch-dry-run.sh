#!/usr/bin/env bash

set -eo pipefail
IFS=$'\n\t'

# This script takes two arguments in the json input
#   manifest: the content of the patch manifest be diffed against what's online
#   kubeconfig: the path to the kubeconfig file
#   namespace:  the namespcace where the object to be patched lives
#   patch object type: the type of object to be patched
#   patch object name: the name of the object to be patched
#   patch type: the type of patch operation

# Extract "manifest" and "kubeconfig" arguments from the input into
# MANIFEST, KUBECONFIG, NAMESPACE, PATCH_OBJECT_TYPE, PATCH_OBJECT_NAME, PATCH_TYPE shell variables.
# jq will ensure that the values are properly quoted
# and escaped for consumption by the shell.
eval "$(jq -r '@sh "MANIFEST=\(.manifest) KUBECONFIG=\(.kubeconfig) NAMESPACE=\(.namespace) PATCH_OBJECT_TYPE=\(.patch_object_type) PATCH_OBJECT_NAME=\(.patch_object_name) PATCH_TYPE=\(.patch_type)"')"

DRIFT="$( kubectl patch --kubeconfig ${KUBECONFIG} -n ${NAMESPACE} ${PATCH_OBJECT_TYPE} ${PATCH_OBJECT_NAME} --type ${PATCH_TYPE} --patch "${MANIFEST}" --dry-run=client  || true )"
if [[ "${DRIFT}" = *"patched (no change)"* ]]; then
  CHANGED="false"
else
  CHANGED="true"
fi

# Safely produce a JSON object containing the result value.
# jq will ensure that the value is properly quoted
# and escaped to produce a valid JSON string.
jq -n --arg changed "${CHANGED}" '{"changed":$changed}'
