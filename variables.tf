variable "enabled" {
  description = "Whether to create the resources in the module"
  type        = bool
  default     = true
}

variable "kubeconfig" {
  description = "Value for KUBECONFIG on the kubectl environment (when `null` value is not set)"
  type        = string
  default     = null
}

variable "namespace" {
  description = "The k8s namespace where the object to patch lives"
  type        = string
  default     = "default"
}

variable "patch_object_type" {
  description = "The type of the object to be patche (eg. pod, job, etc)"
  type        = string
}

variable "patch_object_name" {
  description = "The name of the specific object to be patched"
  type        = string
}

variable "patch_manifest" {
  description = "The k8s manifest to use in patching the object"
  type        = string
}

variable "patch_type" {
  description = "The type of kubectl patch"
  type        = string
  default     = "merge"
}

variable "patch_trigger" {
  description = "Triggers for apply, whenever the string changes apply will trigger"
  default     = " "
}

variable "silence_patch_errors" {
  description = "Whether the module is configured to not fail even when the kubectl patch fails"
  type        = bool
  default     = true
}

variable "detect_drift" {
  description = "Whether to try and detect drift to trigger apply"
  type        = bool
  default     = false
}

variable "rollout_restart" {
  description = "Whether to trigger a `kubectl rollout restart` whenever the patch is applied"
  type        = bool
  default     = false
}

variable "rollout_restart_k8s_resources" {
  description = "A set of fully qualified k8s resources (as expected by `kubectl rollout restart`) to be restarted"
  type        = set(string)
  default     = []
}
