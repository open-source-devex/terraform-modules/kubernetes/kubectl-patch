################################################################################################
# Patch (with/without) silencing errors
################################################################################################
resource "null_resource" "patch_with_errors" {
  count = local.patch_with_errors ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig
      MANIFEST   = var.patch_manifest
    }

    command = "kubectl patch -n ${var.namespace} ${var.patch_object_type} ${var.patch_object_name} --type ${var.patch_type} --patch \"$${MANIFEST}\""
  }

  triggers = merge(local.triggers_detect_drift, {
    namespace         = var.namespace
    patch_object_type = var.patch_object_type
    patch_object_name = var.patch_object_name
    patch_type        = var.patch_type
    external          = var.patch_trigger
  })
}

resource "null_resource" "patch_without_errors" {
  count = local.patch_without_errors ? 1 : 0

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig
      MANIFEST   = var.patch_manifest
    }

    command = "kubectl patch -n ${var.namespace} ${var.patch_object_type} ${var.patch_object_name} --type ${var.patch_type} --patch \"$${MANIFEST}\" || true"
  }

  triggers = merge(local.triggers_detect_drift, {
    namespace         = var.namespace
    patch_object_type = var.patch_object_type
    patch_object_name = var.patch_object_name
    patch_type        = var.patch_type
    external          = var.patch_trigger
  })
}
