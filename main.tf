locals {
  patch_with_errors    = var.enabled && ! var.silence_patch_errors
  patch_without_errors = var.enabled && var.silence_patch_errors

  rollout_restart_with_errors    = local.patch_with_errors && var.rollout_restart
  rollout_restart_without_errors = local.patch_without_errors && var.rollout_restart
}
