################################################################################################
#
#     Logic to trigger a `kubectl rollout restart` when a patch is applied.
#
#   Rollout restart of resources is bound to the same namespace as the patch.
#   Having this logic in this module allows us to create a explicit resource dependency,
#   using `depends_on`, to ensure ordering of execution.
#
#
################################################################################################
resource "null_resource" "rollout_restart_with_errors" {
  for_each = local.rollout_restart_with_errors ? var.rollout_restart_k8s_resources : []

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig
    }

    command = "kubectl rollout restart -n ${var.namespace} ${each.key}"
  }

  triggers = merge(try(null_resource.patch_with_errors[0].triggers, {}), {
    namespace  = var.namespace
    kubeconfig = sha1(var.kubeconfig)
  })

  depends_on = [null_resource.patch_with_errors]
}

resource "null_resource" "rollout_restart_without_errors" {
  for_each = local.rollout_restart_without_errors ? var.rollout_restart_k8s_resources : []

  provisioner "local-exec" {
    when = create

    environment = {
      KUBECONFIG = var.kubeconfig
    }

    command = "kubectl rollout restart -n ${var.namespace} ${each.key} || true"
  }

  triggers = merge(try(null_resource.patch_without_errors[0].triggers, {}), {
    namespace  = var.namespace
    kubeconfig = sha1(var.kubeconfig)
  })

  depends_on = [null_resource.patch_without_errors]
}
